module gitlab.com/zephinzer/akounts

go 1.13

require (
	github.com/go-sql-driver/mysql v1.5.0
	github.com/google/uuid v1.1.1
	github.com/gorilla/mux v1.7.3
	github.com/prometheus/client_golang v1.3.0
	github.com/sanity-io/litter v1.2.0
	github.com/sirupsen/logrus v1.4.2
	github.com/spf13/cobra v0.0.5
	github.com/spf13/viper v1.3.2
	github.com/stretchr/testify v1.4.0
	github.com/usvc/go-log v0.2.3
	golang.org/x/crypto v0.0.0-20200117160349-530e935923ad
)
