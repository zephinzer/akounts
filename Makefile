PROJECT_NAME=akounts
CMD_ROOT=akounts
DOCKER_NAMESPACE=zephinzer
DOCKER_IMAGE_NAME=$(CMD_ROOT)

run:
	go run ./cmd/$(CMD_ROOT)
test:
	go test ./...
build:
	go build -o ./bin/$(CMD_ROOT) ./cmd/$(CMD_ROOT)_${GOOS}_${GOARCH}
build_production:
	CGO_ENABLED=0 \
	go build \
		-a \
		-ldflags "-X main.Commit=$$(git rev-parse --verify HEAD) \
			-X main.Version=$$(git describe --tag $$(git rev-list --tags --max-count=1)) \
			-X main.Timestamp=$$(date +'%Y%m%d%H%M%S') \
			-extldflags 'static' \
			-s -w" \
		-o ./bin/$(CMD_ROOT)_$$(go env GOOS)_$$(go env GOARCH) \
		./cmd/$(CMD_ROOT);


deps:
	go mod vendor -v
	go mod tidy -v
init:
	docker stack deploy --compose-file ./deploy/docker-compose.yml $(PROJECT_NAME)
denit:
	docker stack rm $(PROJECT_NAME)


package:
	docker build --file ./deploy/Dockerfile --tag $(DOCKER_NAMESPACE)/$(DOCKER_IMAGE_NAME):latest .
save:
	mkdir -p ./build
	docker save --output ./build/akounts.tar.gz $(DOCKER_NAMESPACE)/$(DOCKER_IMAGE_NAME):latest
load:
	docker load --input ./build/akounts.tar.gz
publish_dockerhub:
	docker push $(DOCKER_NAMESPACE)/$(DOCKER_IMAGE_NAME):latest

see_ci:
	xdg-open https://gitlab.com/zephinzer/akounts/pipelines