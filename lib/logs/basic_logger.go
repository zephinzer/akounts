package logs

import (
	"fmt"

	"github.com/usvc/go-log/pkg/logger"
)

type BasicLogger func(string, ...interface{})

var DefaultBasicLogger = func(message string, args ...interface{}) {
	fmt.Printf(message, args...)
}

type LevelLogger interface {
	Trace(...interface{})
	Tracef(string, ...interface{})
	Debug(...interface{})
	Debugf(string, ...interface{})
	Info(...interface{})
	Infof(string, ...interface{})
	Warn(...interface{})
	Warnf(string, ...interface{})
	Error(...interface{})
	Errorf(string, ...interface{})
}

var DefaultLevelLogger LevelLogger = logger.New()
