package errors

import (
	"encoding/json"
	"fmt"
	"net/http"
)

const (
	DatabaseCreateTable            = "DB_CREATE_TBL_OP"
	DatabaseDelete                 = "DB_DELETE_OP"
	DatabaseDrop                   = "DB_DROP_OP"
	DatabaseInsert                 = "DB_INSERT_OP"
	DatabaseResultScan             = "DB_SCAN"
	DatabaseSelect                 = "DB_SELECT_OP"
	DatabaseStatementPrep          = "DB_STMT_PREPARE"
	DatabaseUpdate                 = "DB_UPDATE_OP"
	DatabaseConnectionNotAvailable = "DB_CONNECTION_NOT_AVAILABLE"
	JSONParsing                    = "JSON_PARSING"
	OK                             = "OK"
	RequestBodyEmpty               = "REQUEST_BODY_EMPTY"
	RequestBodyParsing             = "REQUEST_BODY_PARSING"
)

// New creates a new error
func New(code string, message string, args ...interface{}) Error {
	return Error{
		Code:    code,
		Message: fmt.Sprintf(message, args...),
	}
}

// Error defines the standard error structure
type Error struct {
	Code    string      `json:"code"`
	Message string      `json:"message"`
	Data    interface{} `json:"data"`
	error
}

// Error ensures Error complies to the error interface
func (e Error) Error() string {
	return fmt.Sprintf("[%s] %s", e.Code, e.Message)
}

// JSON returns a JSON representation of the error
func (e Error) JSON() []byte {
	output, err := json.Marshal(e)
	if err != nil {
		return []byte(fmt.Sprintf(`{"code":"%s", "message":"error while creating JSON output", "data": "%v"}`, JSONParsing, e))
	}
	return output
}

func (e Error) RespondServerError(w http.ResponseWriter) {
	w.WriteHeader(http.StatusInternalServerError)
	w.Write(e.JSON())
}
