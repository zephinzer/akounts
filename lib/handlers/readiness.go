package handlers

import (
	"encoding/json"
	"fmt"
	"net/http"

	"gitlab.com/zephinzer/akounts/lib/errors"
)

func GetReadinessCheck(checks ...func() (string, error)) func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		ready := true
		responseBody := map[string]error{}
		for i := range checks {
			checkKey, err := checks[i]()
			responseBody[checkKey] = errors.New(errors.OK, "ok")
			if err != nil {
				responseBody[checkKey] = err
			}
			if err != nil {
				ready = false
			}
		}
		if !ready {
			w.WriteHeader(http.StatusInternalServerError)
		}
		responseJSON, marshalResponseError := json.Marshal(responseBody)
		if marshalResponseError != nil {
			responseJSON = errors.New(errors.JSONParsing, fmt.Sprintf("failed to parse '%v'", responseBody)).JSON()
		}
		w.Write(responseJSON)
	}
}
