package account

import "time"

// TableName defines the data store table name to use when
// storing information of the Account abstraction
const TableName = "account"

// Account defines a user's primary account
type Account interface {
	GetDatabaseID() uint64
	GetUUID() string
	GetUsername() string
	GetEmail() string
	GetHashedPassword() string
	GetLastUpdated() time.Time
	GetCreatedOn() time.Time

	SetPassword(string) error
	SetUsername(string) error
	SetEmail(string) error

	Create() error
	LoadEmail(string) error
	LoadUsername(string) error
	LoadUUID(string) error
	Update() error

	JSON() []byte
}
