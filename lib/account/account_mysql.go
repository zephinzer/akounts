package account

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"strings"
	"time"

	"github.com/google/uuid"
	"github.com/sanity-io/litter"
	"gitlab.com/zephinzer/akounts/lib/auth"
	"gitlab.com/zephinzer/akounts/lib/errors"
	"gitlab.com/zephinzer/akounts/lib/validate"
)

// NewMySQLAccount creates a new Account interface
func NewMySQLAccount(connection *sql.DB) Account {
	return &MySQL{
		connection: connection,
	}
}

// MySQL defines an implementation for the Account interface that
// includes methods to interact with a MySQL data store
type MySQL struct {
	ID          uint64    `json:"id"`
	UUID        string    `json:"uuid"`
	Email       string    `json:"email"`
	Username    string    `json:"username"`
	Password    string    `json:"password"`
	CreatedOn   time.Time `json:"created_on"`
	LastUpdated time.Time `json:"last_updated"`

	connection *sql.DB
}

// JSON returns the JSON representation of this account instance
func (a MySQL) JSON() []byte {
	output, err := json.Marshal(a)
	if err != nil {
		output = errors.New(errors.JSONParsing, "failed to parse following data as JSON: %v", a).JSON()
	}
	return output
}

// GetDatabaseID complies to the Account interface
func (a MySQL) GetDatabaseID() uint64 {
	return a.ID
}

// GetUUID complies to the Account interface
func (a MySQL) GetUUID() string {
	return a.UUID
}

// GetEmail complies to the Account interface
func (a MySQL) GetEmail() string {
	return a.Email
}

// GetUsername complies to the Account interface
func (a MySQL) GetUsername() string {
	return a.Username
}

// GetHashedPassword complies to the Account interface
func (a MySQL) GetHashedPassword() string {
	return a.Password
}

// GetLastUpdated complies to the Account interface
func (a MySQL) GetLastUpdated() time.Time {
	return a.LastUpdated
}

// GetCreatedOn complies to the Account interface
func (a MySQL) GetCreatedOn() time.Time {
	return a.CreatedOn
}

// SetPassword sets the Password property to a string that
// contains a hash and it's corresponding salt, and is required
// to comply with the Account interface
func (a *MySQL) SetPassword(plaintext string) error {
	if err := validate.Password(plaintext); err != nil {
		return err
	}
	hash, salt, err := auth.HashPlaintext(plaintext)
	if err != nil {
		return err
	}
	a.Password = fmt.Sprintf("%s.%s", hash, salt)
	return nil
}

// SetUsername sets the Username property of the account instance
// and is needed to comply to the Account interface
func (a *MySQL) SetUsername(username string) error {
	if err := validate.Username(username); err != nil {
		return err
	}
	a.Username = username
	return nil
}

// SetEmail sets the Email property of the account instance
// and is needed to comply to the Account interface
func (a *MySQL) SetEmail(email string) error {
	if err := validate.Email(email); err != nil {
		return err
	}
	a.Email = email
	return nil
}

// LoadEmail loads the user based on the provided email :email
func (a *MySQL) LoadEmail(email string) error {
	return a.loadBy("email", email)
}

// LoadUsername loads the user based on the provided username :username
func (a *MySQL) LoadUsername(username string) error {
	return a.loadBy("username", username)
}

// LoadUUID loads the user based on the provided UUID :uuid
func (a *MySQL) LoadUUID(uuid string) error {
	return a.loadBy("uuid", uuid)
}

// Create inserts the account into the data store and
// populates this instance with the generated ID and UUID
func (a *MySQL) Create() error {
	fields := []string{
		"uuid",
		"email",
		"username",
		"password",
	}
	values := []string{}
	for range fields {
		values = append(values, "?")
	}
	a.UUID = uuid.New().String()
	if stmt, err := a.connection.Prepare(fmt.Sprintf(
		"INSERT INTO %s (%s) VALUES (%s)",
		TableName,
		strings.Join(fields, ","),
		strings.Join(values, ","),
	)); err != nil {
		return errors.New(errors.DatabaseStatementPrep, err.Error())
	} else if _, err := stmt.Exec(
		a.UUID,
		a.Email,
		a.Username,
		a.Password,
	); err != nil {
		return errors.New(errors.DatabaseInsert, err.Error())
	} else if stmt, err = a.connection.Prepare(fmt.Sprintf(
		"SELECT id, created_on, last_updated FROM %s WHERE uuid = ?",
		TableName,
	)); err != nil {
		return errors.New(errors.DatabaseStatementPrep, err.Error())
	} else if row := stmt.QueryRow(a.UUID); row == nil {
		return errors.New(errors.DatabaseSelect, "row empty after insertion, weird")
	} else if err = row.Scan(&a.ID, &a.CreatedOn, &a.LastUpdated); err != nil {
		return errors.New(errors.DatabaseResultScan, err.Error())
	}
	return nil
}

// Update updates the data store with data from this
// account instance
func (a *MySQL) Update() error {
	if len(a.UUID) == 0 || a.ID == 0 {
		return errors.New("ACCOUNT_UPDATE_VALIDATION", "ID and UUID field has not been set, if the account exists, load the account first, otherwise, create it")
	}
	fields := []string{
		"email",
		"username",
		"password",
		"last_updated",
	}
	updateString := fmt.Sprintf(
		"UPDATE %s SET %s = ? WHERE id = ? OR uuid = ?",
		TableName,
		strings.Join(fields, " = ?, "),
	)
	litter.Dump(updateString)
	if stmt, err := a.connection.Prepare(updateString); err != nil {
		return errors.New(errors.DatabaseStatementPrep, err.Error())
	} else if _, err := stmt.Exec(
		a.Email,
		a.Username,
		a.Password,
		nil,
		a.ID,
		a.UUID,
	); err != nil {
		return errors.New(errors.DatabaseUpdate, err.Error())
	}
	return nil
}

// loadBy loads this instance of account with data from the
// data source using a where matcher based on the :selectByColumn
// and :selectionValue
func (a *MySQL) loadBy(selectByColumn string, selectionValue string) error {
	fields := []string{
		"id",
		"uuid",
		"email",
		"username",
		"password",
		"created_on",
		"last_updated",
	}
	if stmt, err := a.connection.Prepare(fmt.Sprintf(
		"SELECT %s FROM %s WHERE %s = ?",
		strings.Join(fields, ","),
		TableName,
		selectByColumn,
	)); err != nil {
		return errors.New("DB_STMT", err.Error())
	} else if rows, err := stmt.Query(selectionValue); err != nil {
		return errors.New("DB_SELECT", err.Error())
	} else if rows.Next() {
		if err = rows.Scan(
			&a.ID,
			&a.UUID,
			&a.Email,
			&a.Username,
			&a.Password,
			&a.CreatedOn,
			&a.LastUpdated,
		); err != nil {
			return errors.New("DB_SCAN", err.Error())
		}
	} else {
		return errors.New("USER_NOT_FOUND", "User with %s = %s could not be found", selectByColumn, selectionValue)
	}
	return nil
}
