package api

import (
	"database/sql"
	"encoding/json"
	"io/ioutil"
	"net/http"

	"gitlab.com/zephinzer/akounts/lib/account"
	"gitlab.com/zephinzer/akounts/lib/errors"
	"gitlab.com/zephinzer/akounts/lib/logs"
	"gitlab.com/zephinzer/akounts/lib/validate"
)

// GetPOSTHandler is a wrapper that handles passing of the database
// connection instance to the required handler
func GetPOSTHandler(connection *sql.DB, logger logs.LevelLogger) (string, http.HandlerFunc) {
	return "", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Add("Content-Type", "application/json")

		// data preparation
		rawAccount := map[string]interface{}{}
		requestBody, err := getRequestBody(w, r)
		if err != nil {
			return
		}
		rawAccount, err = getRequestBodyStructure(w, r, requestBody)
		if err != nil {
			return
		}

		// serialization
		acc := account.NewMySQLAccount(connection)
		var email string
		var username string
		var password string
		var ok bool
		if email, ok = rawAccount["email"].(string); !ok {
			email = ""
		}
		if username, ok = rawAccount["username"].(string); !ok {
			username = ""
		}
		if password, ok = rawAccount["password"].(string); !ok {
			password = ""
		}
		if len(email)+len(username) == 0 {
			errors.New(
				"ACCOUNT_NEED_EMAIL_OR_USERNAME",
				"email/username not provided",
			).RespondServerError(w)
			return
		}
		if len(email) > 0 {
			if validationError := validate.Email(email); validationError != nil {
				validationError.(errors.Error).RespondServerError(w)
				return
			} else {
				acc.SetEmail(email)
			}
		}
		if len(username) > 0 {
			if validationError := validate.Username(username); validationError != nil {
				validationError.(errors.Error).RespondServerError(w)
				return
			} else {
				acc.SetUsername(username)
			}
		}
		if len(password) > 0 {
			if validationError := validate.Password(password); validationError != nil {
				validationError.(errors.Error).RespondServerError(w)
			} else {
				acc.SetPassword(password)
			}
		}
		if err := acc.Create(); err != nil {
			errors.New(
				"ACCOUNT_CREATE_FAILED",
				"account could not be created: %s",
				err.Error(),
			).RespondServerError(w)
			return
		}
		logger.Infof("created account[%v] with email/username = '%s/%s'", acc.GetUUID(), acc.GetEmail(), acc.GetUsername())
		w.WriteHeader(http.StatusOK)
		w.Write([]byte(acc.GetUUID()))
	}
}

func getRequestBody(w http.ResponseWriter, r *http.Request) ([]byte, error) {
	requestBody, err := ioutil.ReadAll(r.Body)
	if err != nil {
		errors.New(
			errors.RequestBodyParsing,
			"an error happened while parsing request body: '%s'",
			err.Error(),
		).RespondServerError(w)
		return []byte{}, err
	}
	if len(requestBody) == 0 {
		err := errors.New(
			errors.RequestBodyEmpty,
			"request body not found",
		)
		err.RespondServerError(w)
		return []byte{}, err
	}
	return requestBody, nil
}

func getRequestBodyStructure(w http.ResponseWriter, r *http.Request, requestBody []byte) (map[string]interface{}, error) {
	var unstructuredBodyStructure map[string]interface{}
	if jsonParseError := json.Unmarshal(requestBody, &unstructuredBodyStructure); jsonParseError != nil {
		err := errors.New(
			errors.JSONParsing,
			"an error happened while parsing '%s': '%s'",
			string(requestBody),
			jsonParseError.Error(),
		)
		err.RespondServerError(w)
		return nil, err
	}
	return unstructuredBodyStructure, nil
}
