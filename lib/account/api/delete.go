package api

import (
	"database/sql"
	"gitlab.com/zephinzer/akounts/lib/logs"
	"net/http"
)

// GetDELETEHandler returns the path string and the associated
// handler for that path
func GetDELETEHandler(connection *sql.DB, logger logs.LevelLogger) (string, http.HandlerFunc) {
	return "/{uuid}", func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusTeapot)
		w.Write([]byte("account delete"))
	}
}
