package api

import (
	"database/sql"
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/zephinzer/akounts/lib/account"
	"gitlab.com/zephinzer/akounts/lib/errors"
	"gitlab.com/zephinzer/akounts/lib/logs"
	"gitlab.com/zephinzer/akounts/lib/validate"
)

func GetGETHandler(connection *sql.DB, logger logs.LevelLogger) (string, http.HandlerFunc) {
	return "/{key}", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		params := mux.Vars(r)
		key := params["key"]
		acc := account.NewMySQLAccount(connection)
		switch true {
		case validate.UUID(key) == nil:
			acc.LoadUUID(key)
		case validate.Email(key) == nil:
			acc.LoadEmail(key)
		case validate.Username(key) == nil:
			acc.LoadUsername(key)
		default:
			w.WriteHeader(http.StatusBadRequest)
			w.Write(errors.New(ErrorInvalidAccountRef, fmt.Sprintf(
				"provided account reference '%s' is invalid",
				key,
			)).JSON())
			return
		}
		w.Write(acc.JSON())
	}
}
