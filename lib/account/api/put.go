package api

import (
	"database/sql"
	"net/http"

	"gitlab.com/zephinzer/akounts/lib/logs"
)

func GetPUTHandler(connection *sql.DB, logger logs.LevelLogger) (string, http.HandlerFunc) {
	return "/{uuid}", func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusTeapot)
		w.Write([]byte("account update"))
	}
}
