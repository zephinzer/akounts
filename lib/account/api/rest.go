package api

import (
	"database/sql"

	"github.com/gorilla/mux"
	"gitlab.com/zephinzer/akounts/lib/logs"
)

// AddRestfulHandler adds handlers to the provided :router
func AddRestfulHandler(router *mux.Router, connection *sql.DB, loggers ...logs.LevelLogger) {
	if len(loggers) > 0 {
		log = loggers[0]
	}
	router.HandleFunc(GetPOSTHandler(connection, log)).Methods("POST")
	router.HandleFunc(GetGETHandler(connection, log)).Methods("GET")
	router.HandleFunc(GetPUTHandler(connection, log)).Methods("PUT")
	router.HandleFunc(GetDELETEHandler(connection, log)).Methods("DELETE")
}
