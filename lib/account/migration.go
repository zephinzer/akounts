package account

import (
	"database/sql"
	"gitlab.com/zephinzer/akounts/lib/errors"
	"strings"
)

func MigrateMySQLTable(connection *sql.DB) []error {
	// create the migrations log table
	stmt, err := connection.Prepare(`
		CREATE TABLE IF NOT EXISTS account_migrations(
			id INTEGER UNSIGNED PRIMARY KEY AUTO_INCREMENT,
			content TEXT NOT NULL,
			created_on TIMESTAMP NOT NULL DEFAULT NOW()
		) ENGINE=InnoDB default charset utf8mb4;
	`)
	if err != nil {
		return []error{errors.New(errors.DatabaseStatementPrep, err.Error())}
	}
	_, err = stmt.Exec()
	if err != nil {
		return []error{errors.New(errors.DatabaseCreateTable, err.Error())}
	}

	// actual migrations are here
	accountMigrations := []strings.Builder{}
	migration0001 := strings.Builder{}
	migration0001.Write([]byte("CREATE TABLE IF NOT EXISTS account("))
	migration0001.Write([]byte("id INTEGER UNSIGNED PRIMARY KEY NOT NULL AUTO_INCREMENT,"))
	migration0001.Write([]byte("uuid VARCHAR(36) UNIQUE NOT NULL,"))
	migration0001.Write([]byte("email VARCHAR(256) UNIQUE,"))
	migration0001.Write([]byte("username VARCHAR(64) UNIQUE,"))
	migration0001.Write([]byte("password TEXT NOT NULL,"))
	migration0001.Write([]byte("last_updated TIMESTAMP NOT NULL DEFAULT NOW(),"))
	migration0001.Write([]byte("created_on TIMESTAMP NOT NULL DEFAULT NOW()"))
	migration0001.Write([]byte(") ENGINE=InnoDB default charset utf8mb4;"))
	accountMigrations = append(accountMigrations, migration0001)

	// retrieve migrations to apply
	stmt, err = connection.Prepare(`SELECT id - 1 FROM account_migrations`)
	if err != nil {
		return []error{errors.New(errors.DatabaseStatementPrep, err.Error())}
	}
	rows, err := stmt.Query()
	if err != nil {
		return []error{errors.New(errors.DatabaseSelect, err.Error())}
	}
	appliedMigrations := []int{}
	for rows.Next() {
		var migrationID int
		rows.Scan(&migrationID)
		appliedMigrations = append(appliedMigrations, migrationID)
	}
	migrationsToApply := []string{}
	for i := 0; i < len(accountMigrations); i++ {
		hasBeenApplied := false
		for index := range appliedMigrations {
			if appliedMigrations[index] == i {
				hasBeenApplied = true
			}
		}
		if !hasBeenApplied {
			migrationsToApply = append(migrationsToApply, accountMigrations[i].String())
		}
	}

	// perform the migrations that should be applied
	results := []error{}
	for i := 0; i < len(migrationsToApply); i++ {
		migration := migrationsToApply[i]
		stmt, err := connection.Prepare(migration)
		if err != nil {
			results = append(results, err)
		} else if _, err = stmt.Exec(); err != nil {
			results = append(results, err)
		} else {
			stmt, err = connection.Prepare("INSERT INTO account_migrations (content) VALUES (?)")
			if err != nil {
				results = append(results, err)
			} else if _, err = stmt.Exec(migration); err != nil {
				results = append(results, err)
			} else {
				results = append(results, nil)
			}
		}
	}

	return results
}
