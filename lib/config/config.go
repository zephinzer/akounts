package config

import (
	"github.com/spf13/viper"
	defaultLog "log"
)

type Config struct {
	DatabaseHost       string
	DatabasePort       int
	DatabasePassword   string
	DatabaseUser       string
	DatabaseName       string
	RouteLivenessPath  string
	RouteMetricsPath   string
	RouteReadinessPath string
	ServerInterface    string
	ServerPort         string
}

// Var exports the environment variables after Init() has
// been called
var Var Config

// Init initialises the configuration from the environment
func Init() {
	c := viper.New()
	c.SetDefault("database_host", "localhost")
	c.SetDefault("database_port", 3306)
	c.SetDefault("database_user", "user")
	c.SetDefault("database_password", "password")
	c.SetDefault("database_name", "akounts")
	c.SetDefault("route_liveness_path", "/healthz")
	c.SetDefault("route_metrics_path", "/metrics")
	c.SetDefault("route_readiness_path", "/readyz")
	c.SetDefault("server_port", "2228")
	c.SetDefault("server_interface", "0.0.0.0")
	c.AutomaticEnv()

	Var.DatabaseHost = c.GetString("database_host")
	Var.DatabasePort = c.GetInt("database_port")
	Var.DatabaseUser = c.GetString("database_user")
	Var.DatabasePassword = c.GetString("database_password")
	Var.DatabaseName = c.GetString("database_name")
	Var.RouteLivenessPath = c.GetString("route_liveness_path")
	Var.RouteMetricsPath = c.GetString("route_metrics_path")
	Var.RouteReadinessPath = c.GetString("route_readiness_path")
	Var.ServerPort = c.GetString("server_port")
	Var.ServerInterface = c.GetString("server_interface")
}

// Print displays the consumed configuration
func Print(print func(string, ...interface{})) {
	if print == nil {
		print = defaultLog.Printf
	}
	print("route_liveness_path  : %s\n", Var.RouteLivenessPath)
	print("route_metrics_path   : %s\n", Var.RouteMetricsPath)
	print("route_readiness_path : %s\n", Var.RouteReadinessPath)
	print("server_port          : %s\n", Var.ServerPort)
	print("server_interface     : %s\n", Var.ServerInterface)
}
