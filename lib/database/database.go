package database

import (
	"database/sql"
	"fmt"
	"net/url"
	"strings"

	_ "github.com/go-sql-driver/mysql"
)

type ConnectionOptions struct {
	Hostname string
	Port     int
	Username string
	Password string
	Database string
	Params   map[string]string
}

type Connection struct {
	Instance *sql.DB
	Error    error
	options  *ConnectionOptions
}

func CreateConnection(options ConnectionOptions) Connection {
	connection := Connection{options: &options}
	var params url.URL
	query := params.Query()
	query.Add("parseTime", "true")
	for key, value := range options.Params {
		query.Add(key, value)
	}
	connectionParams := query.Encode()
	connection.Instance, connection.Error = sql.Open(
		"mysql",
		strings.Trim(fmt.Sprintf(
			"%s:%s@tcp(%s:%v)/%s?%s",
			options.Username,
			options.Password,
			options.Hostname,
			options.Port,
			options.Database,
			connectionParams,
		), "/"),
	)
	return connection
}
