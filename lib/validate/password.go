package validate

import (
	"gitlab.com/zephinzer/akounts/lib/errors"
	"math"
)

// DefaultPasswordMinimumDifferentCaseCount defines the default for
// the number of differently cased characters required
const DefaultPasswordMinimumDifferentCaseCount int = 0

// DefaultPasswordMaximumLength defines the default for the length
// of the password
const DefaultPasswordMaximumLength int = 64

// DefaultPasswordMinimumLength defines the default for the minimum
// length of the password
const DefaultPasswordMinimumLength int = 8

// DefaultPasswordMinimumNumericCount defines the default
// number of numeric characters in the password
const DefaultPasswordMinimumNumericCount int = 0

// DefaultPasswordMinimumSpecialCount defines the default
// number of special characters in the password
const DefaultPasswordMinimumSpecialCount int = 0

// DefaultPasswordCustomSpecial defines the default character set
// that is used to define special characters
const DefaultPasswordCustomSpecial = SpecialCharacters

// PasswordPolicy defines possible configurations for password
// requirements
type PasswordPolicy struct {
	MaximumLength             int
	MinimumLength             int
	MinimumDifferentCaseCount int
	MinimumNumericCount       int
	MinimumSpecialCount       int
	CustomSpecial             []byte
}

// GetDefaultPasswordPolicy returns a PasswordPolicy with
// its values set to the default
func GetDefaultPasswordPolicy() PasswordPolicy {
	return PasswordPolicy{
		MaximumLength:             DefaultPasswordMaximumLength,
		MinimumLength:             DefaultPasswordMinimumLength,
		MinimumDifferentCaseCount: DefaultPasswordMinimumDifferentCaseCount,
		MinimumNumericCount:       DefaultPasswordMinimumNumericCount,
		MinimumSpecialCount:       DefaultPasswordMinimumSpecialCount,
		CustomSpecial:             []byte(DefaultPasswordCustomSpecial),
	}
}

// Password validates a provided plaintext password using the
// default PasswordPolicy or a custom policy if it's provided
func Password(plaintext string, customPolicy ...PasswordPolicy) error {
	policy := GetDefaultPasswordPolicy()
	if len(customPolicy) > 0 {
		policy = customPolicy[0]
	}

	passwordMetadata := GetStringMetadata(plaintext, policy.CustomSpecial)
	if passwordMetadata.Length < policy.MinimumLength {
		return errors.New("PASSWORD_TOO_SHORT", "provided password requires at least %v characters", policy.MinimumLength)
	}
	if passwordMetadata.Length > policy.MaximumLength {
		return errors.New("PASSWORD_TOO_LONG", "provided password exceeds the maximum length of %v characters", policy.MaximumLength)
	}
	if math.Abs(float64(passwordMetadata.Lowercases.Len()-passwordMetadata.Uppercases.Len())) < float64(policy.MinimumDifferentCaseCount) {
		return errors.New("PASSWORD_LACKS_CASES", "provided password requires at least %v character(s) of different casings", policy.MinimumDifferentCaseCount)
	}
	if passwordMetadata.Numerics.Len() < policy.MinimumNumericCount {
		return errors.New("PASSWORD_LACKS_NUMERICS", "provided password requires at least %v numeric characters", policy.MinimumNumericCount)
	}
	if passwordMetadata.Specials.Len() < policy.MinimumSpecialCount {
		return errors.New("PASSWORD_LACKS_SPECIALS", "provided password requires at least %v special characters", policy.MinimumSpecialCount)
	}

	return nil
}
