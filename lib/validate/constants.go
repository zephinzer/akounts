package validate

// LowercaseCharacters defines lowercase characters
const LowercaseCharacters = "abcdefghijklmnopqrstuvwxyz"

// UppercaseCharacters defines uppercase characters
const UppercaseCharacters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"

// NumericCharacters defines numerical characters
const NumericCharacters = "1234567890"

// SpecialCharacters defines special characters
const SpecialCharacters = "~`!@#$%^&*()_-=+[{]}\\|;:'\"<>./?"
