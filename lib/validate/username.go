package validate

import (
	"gitlab.com/zephinzer/akounts/lib/errors"
)

// DefaultUsernameMaximumLength defines the default maximum length
// of a username
const DefaultUsernameMaximumLength = 64

// DefaultUsernameMinimumLength defines the default minimum length
// of a username
const DefaultUsernameMinimumLength = 4

// DefaultUsernameAllowUppercase defines the default value for whether
// uppercase characters should be allowed in the username
const DefaultUsernameAllowUppercase = true

// DefaultUsernameAllowNumerics defines the default value for whether
// numeric characters should be allowed in the username
const DefaultUsernameAllowNumerics = true

// DefaultUsernameAllowSpecial defines the default value for whether
// special characters should be allowed in the username
const DefaultUsernameAllowSpecial = false

// DefaultUsernameAllowNumericPrefix defines the default value for whether
// numeric characters should be allowed to prefix the username
const DefaultUsernameAllowNumericPrefix = false

// DefaultUsernameAllowNumericSuffix defines the default value for whether
// numeric characters should be allowed to suffix the username
const DefaultUsernameAllowNumericSuffix = false

// DefaultUsernameAllowSpecialPrefix defines the default value for whether
// special characters should be allowed to prefix the username
const DefaultUsernameAllowSpecialPrefix = false

// DefaultUsernameAllowSpecialSuffix defines the default value for whether
// special characters should be allowed to suffix the username
const DefaultUsernameAllowSpecialSuffix = false

// DefaultUsernameCustomSpecial defines the default set of special
// characters used to define a default username policy
const DefaultUsernameCustomSpecial = SpecialCharacters

// UsernamePolicy defines a structure for holding configurations for
// validating a username
type UsernamePolicy struct {
	MaximumLength      int
	MinimumLength      int
	AllowUppercase     bool
	AllowNumerics      bool
	AllowSpecial       bool
	AllowNumericPrefix bool
	AllowNumericSuffix bool
	AllowSpecialPrefix bool
	AllowSpecialSuffix bool
	CustomSpecial      []byte
}

// GetDefaultUsernamePolicy returns a UsernamePolicy structure
// populated with the defaults
func GetDefaultUsernamePolicy() UsernamePolicy {
	return UsernamePolicy{
		MaximumLength:      DefaultUsernameMaximumLength,
		MinimumLength:      DefaultUsernameMinimumLength,
		AllowUppercase:     DefaultUsernameAllowUppercase,
		AllowNumerics:      DefaultUsernameAllowNumerics,
		AllowSpecial:       DefaultUsernameAllowSpecial,
		AllowNumericPrefix: DefaultUsernameAllowNumericPrefix,
		AllowNumericSuffix: DefaultUsernameAllowNumericSuffix,
		AllowSpecialPrefix: DefaultUsernameAllowSpecialPrefix,
		AllowSpecialSuffix: DefaultUsernameAllowSpecialSuffix,
		CustomSpecial:      []byte(DefaultUsernameCustomSpecial),
	}
}

// Username provides validation for a provided username and returns
// an error if validation fails, otherwise returns <nil>
func Username(username string, customPolicy ...UsernamePolicy) error {
	policy := GetDefaultUsernamePolicy()
	if len(customPolicy) > 0 {
		policy = customPolicy[0]
	}
	usernameMetadata := GetStringMetadata(username, policy.CustomSpecial)
	if usernameMetadata.Length > policy.MaximumLength {
		return errors.New("USERNAME_TOO_LONG", "username (length: %v) cannot be more than %v characters long", usernameMetadata.Length, policy.MaximumLength)
	}
	if usernameMetadata.Length < policy.MinimumLength {
		return errors.New("USERNAME_TOO_SHORT", "username (length: %v) is required to have %v characters", usernameMetadata.Length, policy.MinimumLength)
	}
	if !policy.AllowUppercase && usernameMetadata.Uppercases.Len() > 0 {
		return errors.New("USERNAME_HAS_UPPERCASE", "username cannot have uppercase characters")
	}
	if policy.AllowNumerics {
		if !policy.AllowNumericPrefix && usernameMetadata.PrefixType == StringTypeNumeric {
			return errors.New("USERNAME_HAS_NUMERIC_PREFIX", "username cannot begin with a numeric character")
		}
		if !policy.AllowNumericSuffix && usernameMetadata.SuffixType == StringTypeNumeric {
			return errors.New("USERNAME_HAS_NUMERIC_SUFFIX", "username cannot end with a numeric character")
		}
	} else if usernameMetadata.Numerics.Len() > 0 {
		return errors.New("USERNAME_HAS_NUMERICS", "username cannot have numerical characters")
	}
	if policy.AllowSpecial {
		if !policy.AllowSpecialPrefix && usernameMetadata.PrefixType == StringTypeSpecial {
			return errors.New("USERNAME_HAS_SPECIAL_PREFIX", "username cannot begin with a special character")
		}
		if !policy.AllowSpecialSuffix && usernameMetadata.SuffixType == StringTypeSpecial {
			return errors.New("USERNAME_HAS_SPECIAL_SUFFIX", "username cannot end with a special character")
		}
	} else if usernameMetadata.Specials.Len() > 0 {
		return errors.New("USERNAME_HAS_SPECIALS", "username cannot have special characters (%v)", policy.CustomSpecial)
	}
	return nil
}
