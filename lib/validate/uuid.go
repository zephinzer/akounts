package validate

import "github.com/google/uuid"

// UUID validates whether the provided string is a
// valid UUID, returning true if it is, false otherwise
func UUID(what string) error {
	_, err := uuid.Parse(what)
	return err
}
