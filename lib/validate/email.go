package validate

import (
	"strings"

	"gitlab.com/zephinzer/akounts/lib/errors"
)

// DefaultEmailAllowedDomains defaults to an empty slice
// (this will be split using strings.Split(...) on initiation)
const DefaultEmailAllowedDomains = ""

// DefaultEmailBlockedDomains defaults to an empty slice
// (this will be split using strings.Split(...) on initiation)
const DefaultEmailBlockedDomains = ""

// DefaultEmailBlockedSnippets defaults to an empty slice
// (this will be split using strings.Split(...) on initiation)
const DefaultEmailBlockedSnippets = ""

// DefaultEmailMaximumDomainLevel defaults to domain.com (or 2),
// for example, sub.domain.com will be 3
const DefaultEmailMaximumDomainLevel = 2

// EmailPolicy defines variables available to define a valid
// email adddress for this system
type EmailPolicy struct {
	AllowedDomains     []string
	BlockedDomains     []string
	BlockedSnippets    []string
	MaximumDomainLevel int
}

// GetDefaultEmailPolicy returns an EmailPolicy set to the defaults
func GetDefaultEmailPolicy() EmailPolicy {
	emailPolicy := EmailPolicy{
		MaximumDomainLevel: DefaultEmailMaximumDomainLevel,
	}
	if len(DefaultEmailAllowedDomains) > 0 {
		emailPolicy.AllowedDomains = strings.Split(DefaultEmailAllowedDomains, ",")
	}
	if len(DefaultEmailBlockedDomains) > 0 {
		emailPolicy.BlockedDomains = strings.Split(DefaultEmailBlockedDomains, ",")
	}
	if len(DefaultEmailBlockedSnippets) > 0 {
		emailPolicy.BlockedSnippets = strings.Split(DefaultEmailBlockedSnippets, ",")
	}
	return emailPolicy
}

// Email returns an error if the provided email is not valid,
// otherwise it returns <nil>
func Email(email string, customPolicy ...EmailPolicy) error {
	if err := validateEmailFormat(email); err != nil {
		return err
	}
	policy := GetDefaultEmailPolicy()
	if len(customPolicy) > 0 {
		policy = customPolicy[0]
	}
	if err := validateEmailNoBlockedSnippets(email, policy.BlockedSnippets); err != nil {
		return err
	}
	domain := getEmailDomain(email)
	if err := validateEmailDomainNotBlocked(domain, policy.BlockedDomains); err != nil {
		return err
	}
	if err := validateEmailDomainAllowed(domain, policy.AllowedDomains); err != nil {
		return err
	}
	if err := validateEmailDomainWithinMaximumDomainLevel(domain, policy.MaximumDomainLevel); err != nil {
		return err
	}
	return nil
}

func validateEmailFormat(email string) error {
	sections := strings.Split(email, "@")
	if len(sections) != 2 {
		return errors.New("EMAIL_FORMAT_INVALID", "provided email '%s' has no @", email)
	}
	return nil
}

func validateEmailDomainAllowed(domain string, allowedDomains []string) error {
	if len(allowedDomains) > 0 {
		found := false
		for i := 0; i < len(allowedDomains); i++ {
			allowedDomain := allowedDomains[i]
			if domain == allowedDomain {
				found = true
				break
			}
		}
		if !found {
			return errors.New("EMAIL_NOT_WHITELISTED", "provided email's domain (%s) does not belong to a whitelisted domain", domain)
		}
	}
	return nil
}

func validateEmailDomainNotBlocked(domain string, blockedDomains []string) error {
	if len(blockedDomains) > 0 {
		for i := 0; i < len(blockedDomains); i++ {
			blockedDomain := blockedDomains[i]
			if domain == blockedDomain {
				return errors.New("EMAIL_BLACKLISTED", "provided email belongs to a blocked domain (%s)", domain)
			}
		}
	}
	return nil
}

func validateEmailDomainWithinMaximumDomainLevel(domain string, maxmimumDomainLevel int) error {
	if getDomainLevel(domain) > maxmimumDomainLevel {
		return errors.New("EMAIL_SUBDOMAIN_LEVEL_TOO_HIGH", "provided email has a sub-domain level of > %v (%s)", maxmimumDomainLevel, domain)
	}
	return nil
}

func validateEmailNoBlockedSnippets(email string, blockedSnippets []string) error {
	if len(blockedSnippets) > 0 {
		for i := 0; i < len(blockedSnippets); i++ {
			blockedSnippet := blockedSnippets[i]
			if strings.Index(email, blockedSnippet) != -1 {
				return errors.New("EMAIL_CONTAINS_BLOCKED_SNIPPET", "provided email contains blocked snippet \"%s\"", blockedSnippet)
			}
		}
	}
	return nil
}

func getEmailLocalPart(email string) string {
	deconstructedEmail := strings.Split(email, "@")
	return deconstructedEmail[0]
}

func getEmailDomain(email string) string {
	deconstructedEmail := strings.Split(email, "@")
	return deconstructedEmail[len(deconstructedEmail)-1]
}

func getDomainLevel(domain string) int {
	return len(strings.Split(domain, "."))
}
