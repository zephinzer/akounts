package auth

import (
	"testing"

	"github.com/stretchr/testify/suite"
)

type HashingTests struct {
	suite.Suite
}

func TestHashes(t *testing.T) {
	suite.Run(t, &HashingTests{})
}

func (s *HashingTests) TestEndToEnd() {
	expectedPlaintext := "hihihi"
	unexpectedPlaintexts := []string{
		"hihihh",
		"hihihj",
		"hihih",
		"hihihih",
	}
	hash, salt, _ := HashPlaintext(expectedPlaintext)
	err := ValidateHash(expectedPlaintext, hash, salt)
	s.Nil(err)
	for _, otherPlaintext := range unexpectedPlaintexts {
		err = ValidateHash(otherPlaintext, hash, salt)
		s.NotNil(err)
	}
}
