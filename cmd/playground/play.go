package main

import (
	"database/sql"
	"fmt"
	"net/url"
	"reflect"

	_ "github.com/go-sql-driver/mysql"
	"github.com/sanity-io/litter"
)

func main() {
	var x url.URL
	q := x.Query()
	q.Add("hello", "world")
	q.Add("hello2", "again and again")
	litter.Dump(q.Encode())
	// fmt.Println(migrateTable("account", account.NewMySQLAccount()))
	// conn, err := sql.Open("mysql", "user:password@tcp(localhost:3306)/akounts?parseTime=true")
	// if err != nil {
	// 	panic(err)
	// }
	// acc := account.NewMySQLAccount(conn)
	// if err = acc.SetEmail("email5@domain.com"); err != nil {
	// 	panic(err)
	// }
	// if err = acc.SetPassword("password"); err != nil {
	// 	panic(err)
	// }
	// if err = acc.SetUsername("username5a"); err != nil {
	// 	panic(err)
	// }
	// err = acc.Create()
	// if err != nil {
	// 	panic(err)
	// }
	// if err = acc.SetEmail("email6@domain.com"); err != nil {
	// 	panic(err)
	// }
	// if err = acc.SetUsername("username6a"); err != nil {
	// 	panic(err)
	// }
	// err = acc.Update()
	// if err != nil {
	// 	panic(err)
	// }
	// litter.Dump(account.MigrateMySQLTable(conn))
}

func selectAllFrom(tableName string, columnNames ...string) []map[string]interface{} {
	conn, err := sql.Open("mysql", "user:password@tcp(localhost:3306)/akounts")
	if err != nil {
		panic(err)
	}
	stmt, err := conn.Prepare(fmt.Sprintf("SELECT id, uuid, email, username, password, last_updated, created_on FROM %s", tableName))
	if err != nil {
		panic(err)
	}
	rows, err := stmt.Query()
	if err != nil {
		panic(err)
	}
	var results []map[string]interface{}
	for rows.Next() {
		// row := map[string]interface{}{}
		row := []interface{}{}
		rowMapping := []string{}
		colTypes, _ := rows.ColumnTypes()
		for index := range colTypes {
			colType := colTypes[index].ScanType()
			colName := colTypes[index].Name()
			colPointer := reflect.New(colType)
			row = append(row, colPointer)
			rowMapping = append(rowMapping, colName)
		}
		_ = rows.Scan(
			row...,
		)
		result := map[string]interface{}{}
		for i := 0; i < len(rowMapping); i++ {
			result[rowMapping[i]] = row[i]
		}
		results = append(results, result)
	}
	return results
}

func migrateTable(tableName string, table interface{}) string {
	tableType := reflect.ValueOf(table).Elem()
	migrateAccounts := fmt.Sprintf(`CREATE TABLE IF NOT EXISTS %s (id UNSIGNED INTEGER PRIMARY KEY UNIQUE NOT NULL AUTO_INCREMENT, created_on TIMESTAMP NOT NULL DEFAULT NOW(), last_updated TIMESTAMP NOT NULL DEFAULT NOW()) ENGINE=InnoDB default charset utf8mb4;`, tableName)
	for i := 0; i < tableType.NumField(); i++ {
		field := tableType.Type().Field(i)
		if _, ok := field.Tag.Lookup("col"); ok {
			columnName := field.Tag.Get("col")
			columnType := field.Tag.Get("type")
			columnModifiers := field.Tag.Get("mod")
			migrateAccounts = fmt.Sprintf("%s\nALTER TABLE %s ADD %s %s %s", migrateAccounts, tableName, columnName, columnType, columnModifiers)
			if _, ok := field.Tag.Lookup("comment"); ok {
				columnComment := field.Tag.Get("comment")
				migrateAccounts = fmt.Sprintf(`%s COMMENT "%s"`, migrateAccounts, columnComment)
			}
			migrateAccounts += ";"
		}
	}
	return migrateAccounts
}
