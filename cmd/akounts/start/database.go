package start

import (
	"gitlab.com/zephinzer/akounts/lib/config"
	"gitlab.com/zephinzer/akounts/lib/database"
)

var databaseConnection database.Connection

func initDatabaseConnection() {
	databaseConnection = database.CreateConnection(database.ConnectionOptions{
		Hostname: config.Var.DatabaseHost,
		Port:     config.Var.DatabasePort,
		Username: config.Var.DatabaseUser,
		Password: config.Var.DatabasePassword,
		Database: config.Var.DatabaseName,
	})
}
