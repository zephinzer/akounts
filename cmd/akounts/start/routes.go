package start

import (
	"database/sql"

	"github.com/gorilla/mux"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	accountAPI "gitlab.com/zephinzer/akounts/lib/account/api"
	"gitlab.com/zephinzer/akounts/lib/config"
	"gitlab.com/zephinzer/akounts/lib/errors"
	"gitlab.com/zephinzer/akounts/lib/handlers"
)

var routes *mux.Router

func initRoutes(connection *sql.DB) {
	routes = mux.NewRouter()

	routes.StrictSlash(true)
	routes.HandleFunc("/", handlers.DefaultResponse).Methods("GET")

	// /account handlers
	accountAPIRouter := routes.PathPrefix("/account").Subrouter()
	accountAPI.AddRestfulHandler(accountAPIRouter, connection)

	// liveness checks
	routes.HandleFunc(config.Var.RouteLivenessPath, handlers.LivenessCheck).Methods("GET")

	// readiness checks
	routes.HandleFunc(config.Var.RouteReadinessPath, handlers.GetReadinessCheck(
		func() (string, error) {
			var err error
			if databaseConnection.Error != nil {
				err = errors.New(errors.DatabaseConnectionNotAvailable, "unable to connect to database: %s", databaseConnection.Error.Error())
			}
			return "database", err
		},
	)).Methods("GET")

	// metrics export
	routes.Handle(config.Var.RouteMetricsPath, promhttp.Handler()).Methods("GET")
}
