package start

import (
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"syscall"

	"gitlab.com/zephinzer/akounts/lib/config"
)

var server http.Server

func initServer() {
	addr := fmt.Sprintf("%s:%s", config.Var.ServerInterface, config.Var.ServerPort)
	server = http.Server{
		Addr:    addr,
		Handler: routes,
	}
}

func startServer(exit chan error) {
	exit <- server.ListenAndServe()
}

func stopServerOnOSSignal(exit chan error, onSignal func(os.Signal)) {
	received := make(chan os.Signal, 1)
	signal.Notify(received, syscall.SIGINT, syscall.SIGTERM)
	onSignal(<-received)
	exit <- server.Close()
}
