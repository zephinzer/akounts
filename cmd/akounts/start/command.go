package start

import (
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/zephinzer/akounts/lib/config"
	"gitlab.com/zephinzer/akounts/lib/logs"
)

var log logs.LevelLogger = logs.DefaultLevelLogger

func Get(exit chan error, loggers ...logs.LevelLogger) *cobra.Command {
	return &cobra.Command{
		Use:   "start",
		Short: "Starts the service",
		Run: func(command *cobra.Command, args []string) {
			if len(loggers) > 0 {
				log = loggers[0]
			}
			config.Print(log.Debugf)

			initDatabaseConnection()
			initRoutes(databaseConnection.Instance)
			initServer()
			serverExited := make(chan error, 1)

			go startServer(serverExited)
			go stopServerOnOSSignal(serverExited, func(signal os.Signal) {
				log.Infof("received %s", signal)
			})

			serverError := <-serverExited
			if serverError == nil {
				log.Info("server shutdown with no issues")
			}
			exit <- serverError
		},
	}
}
