package migrate

import (
	"github.com/spf13/cobra"
	"gitlab.com/zephinzer/akounts/lib/account"
	"gitlab.com/zephinzer/akounts/lib/config"
	"gitlab.com/zephinzer/akounts/lib/database"
	"gitlab.com/zephinzer/akounts/lib/logs"
)

var log logs.BasicLogger = logs.DefaultBasicLogger

func Get(exit chan error, logger ...logs.BasicLogger) *cobra.Command {
	return &cobra.Command{
		Use:   "migrate",
		Short: "Runs database migrations",
		Run: func(command *cobra.Command, args []string) {
			if len(logger) > 0 {
				log = logger[0]
			}
			config.Print(log)
			connection := database.CreateConnection(
				database.ConnectionOptions{
					Hostname: config.Var.DatabaseHost,
					Port:     config.Var.DatabasePort,
					Username: config.Var.DatabaseUser,
					Password: config.Var.DatabasePassword,
					Database: config.Var.DatabaseName,
				},
			)
			if connection.Error != nil {
				panic(connection.Error)
			}

			// run migrations here
			if errs := account.MigrateMySQLTable(connection.Instance); len(errs) > 0 {
				for i := 0; i < len(errs); i++ {
					if err := errs[i]; err != nil {
						log(errs[i].Error())
					}
				}
			}
			exit <- nil
		},
	}
}
