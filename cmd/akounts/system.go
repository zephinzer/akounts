package main

import (
	defaultLog "log"
	"runtime"
	"time"
)

func logSystemStatisticsEvery(every <-chan time.Time, logger ...func(string, ...interface{})) {
	for {
		select {
		case <-every:
			memStats := runtime.MemStats{}
			runtime.ReadMemStats(&memStats)
			processLogf := defaultLog.Printf
			if len(logger) > 0 {
				processLogf = logger[0]
			}
			processLogf("alloc: %v, total alloc: %v, heap alloc: %v", memStats.Alloc, memStats.TotalAlloc, memStats.HeapAlloc)
		}
	}
}
