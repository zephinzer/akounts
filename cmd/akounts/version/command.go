package version

import (
	"github.com/spf13/cobra"
	"gitlab.com/zephinzer/akounts/lib/logs"
)

var (
	// Version contains the semantic version of the binary
	Version string
	// Commit contains the commit hash
	Commit string
	// Timestamp indicates when the binary was built
	Timestamp string
)

var log logs.BasicLogger = logs.DefaultBasicLogger

func Get(exit chan error, logger ...logs.BasicLogger) *cobra.Command {
	return &cobra.Command{
		Use:   "version",
		Short: "Display version information",
		Run: func(command *cobra.Command, args []string) {
			if len(logger) > 0 {
				log = logger[0]
			}
			log("%s-%s / %s", Version, Commit, Timestamp)
			exit <- nil
		},
	}
}
