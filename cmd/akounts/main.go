package main

import (
	"os"
	"strings"
	"time"

	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"github.com/usvc/go-log/pkg/logger"
	"gitlab.com/zephinzer/akounts/cmd/akounts/env"
	"gitlab.com/zephinzer/akounts/cmd/akounts/migrate"
	"gitlab.com/zephinzer/akounts/cmd/akounts/start"
	"gitlab.com/zephinzer/akounts/cmd/akounts/version"
	"gitlab.com/zephinzer/akounts/lib/config"
)

var exit chan error
var log *logrus.Logger
var rootCommand *cobra.Command

func init() {
	exit = make(chan error, 1)
	log = logger.New()
	config.Init()

	rootCommand = &cobra.Command{
		Use:   "akounts",
		Short: "A service to handle accounts management for a web application",
		Long: strings.Trim(strings.ReplaceAll(`
		Akounts - Accounts management system

		A service to handle accounts management for a web application
		`, "\n\t", "\n"), " \t\n"),
		Run: func(command *cobra.Command, args []string) {
			command.Help()
			exit <- nil
		},
	}
	rootCommand.AddCommand(start.Get(exit))
	rootCommand.AddCommand(env.Get(exit))
	rootCommand.AddCommand(version.Get(exit))
	rootCommand.AddCommand(migrate.Get(exit))
}

func main() {
	go logSystemStatisticsEvery(time.Tick(10*time.Second), log.Tracef)
	rootCommand.Execute()
	anyError := <-exit
	if anyError != nil {
		os.Exit(1)
	} else {
		os.Exit(0)
	}
}
