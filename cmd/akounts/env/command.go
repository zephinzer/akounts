package env

import (
	"github.com/spf13/cobra"
	"gitlab.com/zephinzer/akounts/lib/config"
	"gitlab.com/zephinzer/akounts/lib/logs"
)

var log logs.BasicLogger = logs.DefaultBasicLogger

func Get(exit chan error, loggers ...logs.BasicLogger) *cobra.Command {
	return &cobra.Command{
		Use:   "env",
		Short: "Check the consumed configuration",
		Run: func(command *cobra.Command, args []string) {
			if len(loggers) > 0 {
				log = loggers[0]
			}
			config.Print(log)
			exit <- nil
		},
	}
}
