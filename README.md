# Akounts

A service to handle accounts management for a web application.

- ✅ User login
- ✅ User logout
- ✅ User sessions
- ✅ User event trail

- - -

# Usage

## Deploying

### Support Services

### Environment Variables

## API Documentation

### Controller endpoints

#### Registering a new user

`POST http://localhost:2228/register`

#### Logging a user in

`POST http://localhost:2228/login`

### Model endpoints

#### Creating accounts

`POST http://localhost:2228/account`

#### Retrieving accounts

`GET http://localhost:2228/account/:uuid`

#### Updating accounts

`PUT http://localhost:2228/account/:uuid`

#### Removing accounts

`DELETE http://localhost:2228/account/:uuid`



#### Creating profiles

`POST http://localhost:2228/profile`

#### Retrieving profiles

`GET http://localhost:2228/profile/:uuid`

#### Updating profiles

`PUT http://localhost:2228/profile/:uuid`

#### Removing profiles

`DELETE http://localhost:2228/profile/:uuid`



#### Creating roles

`POST http://localhost:2228/role`

#### Retrieving roles

`GET http://localhost:2228/role/:uuid`

#### Updating roles

`PUT http://localhost:2228/role/:uuid`

#### Removing roles

`DELETE http://localhost:2228/role/:uuid`

- - -

# Concepts

## Role-Based-Access-Control (RBAC)

- - -

# Licensing

Code in this repository is licensed under [the MIT license](./LICENSE).